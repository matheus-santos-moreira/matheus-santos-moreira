### Hi there, I'm Matheus Santos 👋

## I'm a Husband, Father and Developer!
- 🔭 I’m currently working as a clerk in a bakery, but I am looking for a job as a dev.
- 🌱 I’m currently learning everything, but focused on TypeScript, NodeJS, ReactJS and React Native. 🤣

### Connect with me:
​
<a href="https://linkedin.com/in/matheus-santos-moreira">
<img src="https://img.shields.io/badge/linkedin-%230077B5.svg?&style=for-the-badge&logo=linkedin&logoColor=white ">
</a>
<a href="https://www.instagram.com/matheus.s.moreira/">
<img src="https://img.shields.io/badge/instagram-%23E4405F.svg?&style=for-the-badge&logo=instagram&logoColor=white">
</a>
<p align="left"><img src="https://devicons.github.io/devicon/devicon.git/icons/css3/css3-original-wordmark.svg" alt="css3" width="40" height="40"/> <img src="https://devicons.github.io/devicon/devicon.git/icons/docker/docker-original-wordmark.svg" alt="docker" width="40" height="40"/> <img src="https://www.vectorlogo.zone/logos/git-scm/git-scm-icon.svg" alt="git" width="40" height="40"/> <img src="https://devicons.github.io/devicon/devicon.git/icons/html5/html5-original-wordmark.svg" alt="html5" width="40" height="40"/> <img src="https://devicons.github.io/devicon/devicon.git/icons/javascript/javascript-original.svg" alt="javascript" width="40" height="40"/> <img src="https://devicons.github.io/devicon/devicon.git/icons/mongodb/mongodb-original-wordmark.svg" alt="mongodb" width="40" height="40"/> <img src="https://devicons.github.io/devicon/devicon.git/icons/mysql/mysql-original-wordmark.svg" alt="mysql" width="40" height="40"/> <img src="https://devicons.github.io/devicon/devicon.git/icons/nodejs/nodejs-original-wordmark.svg" alt="nodejs" width="40" height="40"/> <img src="https://devicons.github.io/devicon/devicon.git/icons/postgresql/postgresql-original-wordmark.svg" alt="postgresql" width="40" height="40"/> <img src="https://devicons.github.io/devicon/devicon.git/icons/react/react-original-wordmark.svg" alt="react" width="40" height="40"/> <img src="https://reactnative.dev/img/header_logo.svg" alt="reactnative" width="40" height="40"/> <img src="https://devicons.github.io/devicon/devicon.git/icons/typescript/typescript-original.svg" alt="typescript" width="40" height="40"/></p>



<details>
  <summary>:zap: Github Stats</summary>

<p>&nbsp;<img align="center" src="https://github-readme-stats.vercel.app/api?username=matheus-santos-moreira&show_icons=true" alt="matheus-santos-moreira" /></p>
</details>

<details>
  <summary>:zap: Most Used Languages </summary>
<p><img align="left" src="https://github-readme-stats.vercel.app/api/top-langs/?username=matheus-santos-moreira&layout=compact&hide=html" alt="matheus-santos-moreira" /></p>

</details>

